<?php
require '../../dist/libs/conexion.php';

$data   = $_REQUEST['proyecto'];
$msg    = [];
$folder = '../../dist/proyectos/';

switch ($data['action']) {
    case 'Proyecto-nuevo':
        $check = $db
            ->where('nombre_p', $data['nombre'])
            ->objectBuilder()->get('proyectos');

        if ($db->count == 0) {
            $datos = [
                'nombre_p' => $data['nombre'],
                'estado_p' => $data['estado'],
                'descripcion_p' => $data['describe'],
                'creacion_p' => date('Y-m-d H:i:s')
            ];

            if (isset($data['imagen'])) {
                $img        = explode(',', $data['imagen']);
                $img        = base64_decode($img[1]);
                $nombre     = date('YmdHis') . Limpiar($data['imagen_nombre']) . '.jpg';
                $archivador = $folder . $nombre;

                if (file_put_contents($archivador, $img)) {
                    require 'imagine/vendor/autoload.php';
                    $imagine = new Imagine\Gd\Imagine();

                    $img = $imagine->open($archivador);
                    $img = $img->save($archivador);

                    $datos['imagen_p'] = substr($archivador, 6);
                }
            }

            $nuevo = $db
                ->insert('proyectos', $datos);

            if ($nuevo) {
                $msg['status']   = true;
                $msg['msg'] = 'Proyecto creado';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo crear el proyecto';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el proyecto ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Proyecto-lista':
        $page       = $data['pagina'];
        $results_pg = 30;
        $adjacent   = 2;

        $nombre    = $db->escape(trim($data['nombre']));

        ($nombre == '' ? $nombre = '%' : '');

        $totalitems = $db
            ->where('nombre_p', '%' . $nombre . '%', 'LIKE')
            ->objectBuilder()->get('proyectos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->where('nombre_p', '%' . $nombre . '%', 'LIKE')
                ->orderBy('Id_p', 'DESC')
                ->objectBuilder()->paginate('proyectos', $page);

            foreach ($listing as $proyecto) {
                $estado = '';

                switch ($proyecto->estado_p) {
                    case '0':
                        $estado = 'Inactivo';
                        break;
                    case '1':
                        $estado = 'Activo';
                        break;
                }

                $creacion = explode(' ', $proyecto->creacion_p);
                $creacion = $creacion[0];

                $edicion = '';

                if ($proyecto->edicion_p != '') {
                    $edicion = explode(' ', $proyecto->edicion_p);
                    $edicion = $edicion[0];
                }

                $content .= '<tr id="P-' . $proyecto->Id_p . '">
                                <td>' . $proyecto->Id_p . '</td>
                                <td>' . $proyecto->nombre_p . '</td>
                                <td>' . $creacion . '</td>
                                <td>' . $edicion . '</td>
                                <td><a href="../../' . $proyecto->imagen_p . '" target="_blank" class="waves-effect waves-light btn">Ver</a></td>
                                <td>' . $estado . '</td>
                                <td> <a href="#!" class="waves-effect waves-light btn light-blue darken-2 Editar-proyecto">Editar</a> <a href="#!" class="waves-effect waves-light btn red darken-3 Eliminar-proyecto">Eliminar</a></td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="7">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Proyecto-info':
        $idproyecto = explode('-', $data['idproyecto']);

        $check = $db
            ->where('Id_p', $idproyecto[1])
            ->objectBuilder()->get('proyectos');

        if ($db->count > 0) {
            $proyectos = $db
                ->where('Id_p', $idproyecto[1])
                ->objectBuilder()->get('proyectos');

            $msg['status'] = true;
            $msg['info']   = $proyectos;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el proyecto no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Proyecto-editar':
        $idproyecto = explode('-', $data['idproyecto']);

        $check = $db
            ->where('nombre_p', $data['nombre'])
            ->where('Id_p', $idproyecto[1], '!=')
            ->objectBuilder()->get('proyectos');

        if ($db->count == 0) {
            $datos = [
                'nombre_p' => $data['nombre'],
                'estado_p' => $data['estado'],
                'descripcion_p' => $data['describe'],
                'edicion_p' => date('Y-m-d H:i:s')
            ];

            if (isset($data['imagen'])) {
                $img        = explode(',', $data['imagen']);
                $img        = base64_decode($img[1]);
                $nombre     = date('YmdHis') . Limpiar($data['imagen_nombre']) . '.jpg';
                $archivador = $folder . $nombre;

                if (file_put_contents($archivador, $img)) {
                    require 'imagine/vendor/autoload.php';
                    $imagine = new Imagine\Gd\Imagine();

                    $img = $imagine->open($archivador);
                    $img = $img->save($archivador);

                    $proyectos = $db
                        ->where('Id_p', $idproyecto[1])
                        ->objectBuilder()->get('proyectos');

                    if ($proyectos[0]->imagen_p != '') {
                        if (file_exists('../../' . $proyectos[0]->imagen_p)) {
                            unlink('../../' . $proyectos[0]->imagen_p);
                        }
                    }

                    $datos['imagen_p'] = substr($archivador, 6);
                }
            }

            $editar = $db
                ->where('Id_p', $idproyecto[1])
                ->update('proyectos', $datos);

            if ($editar) {
                $msg['status']   = true;
                $msg['msg'] = 'Proyecto editado';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo editar el proyecto';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el proyecto ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Proyecto-eliminar':
        $idproyecto = explode('-', $data['idproyecto']);

        $check = $db
            ->where('Id_p', $idproyecto[1])
            ->objectBuilder()->get('proyectos');

        if ($db->count > 0) {
            if ($check[0]->imagen_p != '') {
                if (file_exists('../../' . $check[0]->imagen_p)) {
                    unlink('../../' . $check[0]->imagen_p);
                }
            }

            $proyectos = $db
                ->where('Id_p', $idproyecto[1])
                ->delete('proyectos');

            $msg['status'] = true;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el proyecto no existe!';
        }

        echo json_encode($msg);
        break;
}


function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
