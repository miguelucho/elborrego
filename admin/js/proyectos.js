$(document).ready(function () {
    formdata = new FormData();
    bsq_nombre = '';
    var $uploadCrop,
        tempFilename,
        rawImg;

    listado(1);

    editor1 = CKEDITOR.replace('Descripcion', {
        toolbarGroups: [{ name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'styles', groups: ['styles'] },
        { name: 'colors', groups: ['colors'] },
            '/',
        { name: 'insert', groups: ['insert'] },
            '/',
        { name: 'about', groups: ['about'] }
        ],
        removeButtons: 'Save,NewPage,Print,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Source,CreateDiv',
        resize_dir: 'both',
        height: 100,
    });

    editor2 = CKEDITOR.replace('Descripcion-ed', {
        toolbarGroups: [{ name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'styles', groups: ['styles'] },
        { name: 'colors', groups: ['colors'] },
            '/',
        { name: 'insert', groups: ['insert'] },
            '/',
        { name: 'about', groups: ['about'] }
        ],
        removeButtons: 'Save,NewPage,Print,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Source,CreateDiv',
        resize_dir: 'both',
        height: 100,
    });

    editor1.on('change', function (evt) {
        editor1.updateElement();
    });

    editor2.on('change', function (evt) {
        editor2.updateElement();
    });

    $('.imagen-adjunta').on('change', function () {
        if ($(this).val() !== '') {
            if (/^image\/\w+/.test(this.files[0].type)) {
                tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
                tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
                readFile(this);
                modal({
                    type: 'info',
                    title: 'Recortar Imagen',
                    text: '<div class="modal-body">' +
                        '<div id="upload-demo" class="center-block"></div>' +
                        '</div>',
                    size: 'large',
                    callback: function (result) {
                        if (result === true) {
                            $uploadCrop.croppie('result', {
                                type: 'base64',
                                format: 'jpeg',
                                size: 'original',
                                size: {
                                    width: 364,
                                    height: 180
                                },
                            }).then(function (resp) {
                                formdata.append('proyecto[imagen]', resp);
                                formdata.append('proyecto[imagen_nombre]', tempFilename);
                            });
                        }
                    },
                    onShow: function (result) {
                        $uploadCrop = $('#upload-demo').croppie({
                            viewport: {
                                width: 364,
                                height: 180,
                            },
                            enforceBoundary: false,
                            enableExif: true,
                            enableResize: false
                        });
                        $uploadCrop.croppie('bind', {
                            url: rawImg
                        }).then(function () {
                            console.log('jQuery bind complete');
                        });
                    },
                    closeClick: false,
                    animate: true,
                    buttonText: {
                        yes: 'Confirmar',
                        cancel: 'Cancelar'
                    }
                });
            } else {
                notificacion('error', 'No es un formato válido');
                $(this).val('');
            }
        }
    });

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                rawImg = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            notificacion('error', 'Su navegador no soporta FileReader API');
        }
    }

    $('#Proyecto-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();

        otradata = $(this).serializeArray();
        formdata.append('proyecto[action]', 'Proyecto-nuevo');

        $.each(otradata, function (key, input) {
            formdata.append(input.name, input.value);
        });

        $.ajax({
            url: 'lib/ac_proyectos',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    notificacion('correcto', data.msg);
                    $('input', '#Proyecto-nuevo').not('[type=submit]').val('');
                    editor1.setData('');
                    $('textarea', '#Proyecto-nuevo').val('');
                    $("select").formSelect();
                    window.M.updateTextFields();
                    formdata = new FormData();
                    listado(1);
                } else {
                    notificacion('error', data.msg);
                }
            }
        });
    });

    $('body').on('click', '.Editar-proyecto', function () {
        loader();
        proyecto_editar = $(this).closest('tr').prop('id');
        $.post('lib/ac_proyectos', {
            'proyecto[action]': 'Proyecto-info',
            'proyecto[idproyecto]': proyecto_editar
        }, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $.each(data.info, function (i, dat) {
                    $('#Nombre-ed').val(dat.nombre_p);
                    $('#Estado-ed').val(dat.estado_p);
                    $('#Descripcion-ed').val(dat.descripcion_p);
                    window.M.updateTextFields();
                    editor2.setData(dat.descripcion_p);
                    $('#Descripcion-ed').parent().find('label').removeClass('active');
                    M.FormSelect.init(document.querySelectorAll("select"));
                });
                $('#editarproyecto').modal('open');
            } else if (data.status == false) {
                notificacion('error', data.msg);
            }
        }, 'json');
    });

    $('#Proyecto-editar').on('submit', function (e) {
        e.preventDefault();
        if (proyecto_editar != '') {
            loader();

            otradata = $(this).serializeArray();
            formdata.append('proyecto[action]', 'Proyecto-editar');
            formdata.append('proyecto[idproyecto]', proyecto_editar);

            $.each(otradata, function (key, input) {
                formdata.append(input.name, input.value);
            });

            $.ajax({
                url: 'lib/ac_proyectos',
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('#fondo').remove();
                    if (data.status == true) {
                        $('#editarproyecto').modal('close');
                        $('input', '#Proyecto-editar').not('[type=submit]').val('');
                        editor2.setData('');
                        $('textarea', '#Proyecto-editar').val('');
                        M.updateTextFields();
                        $('#Descripcion-ed').parent().find('label').removeClass('active');
                        $("select option[value='']", '#Proyecto-editar').attr('selected', true);
                        proyecto_editar = '';
                        listado(1);
                        notificacion('correcto', data.msg);
                        formdata = new FormData();
                    } else if (data.status == false) {
                        notificacion('error', data.msg);
                    }
                }
            });
        } else {
            notificacion('error', 'Se ha producido un error!');
        }
    });

    $('body').on('click', '.Eliminar-proyecto', function () {
        proyecto_eliminar = $(this).closest('tr').prop('id');
        $.confirm({
            title: 'Eliminar Proyecto',
            content: 'Desea eliminar este proyecto?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-red',
                    action: function () {
                        loader();
                        $.post('lib/ac_proyectos', {
                            'proyecto[action]': 'Proyecto-eliminar',
                            'proyecto[idproyecto]': proyecto_eliminar,
                        }, function (data) {
                            $('#fondo').remove();
                            if (data.status == true) {
                                listado(1);
                            } else if (data.status == false) {
                                notificacion('error', data.msg);
                            }
                        }, 'json');
                    },
                },
                cancelar: function () {

                },
            },
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });

    });

    $('#busqueda').on('click', function () {
        bsq_nombre = $('#buscar-nombre').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#listado-proyectos').empty().fadeOut(0);
        $.post('lib/ac_proyectos', {
            'proyecto[action]': 'Proyecto-lista',
            'proyecto[nombre]': bsq_nombre,
            'proyecto[pagina]': pg
        }, function (data) {
            $('#listado-proyectos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#listado-proyectos').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notificacion(type, msg) {
        switch (type) {
            case 'error':
                color = 'red';
                icono = 'fa fa-warning';
                titulo = 'Error';
                break;
            case 'correcto':
                color = 'green';
                icono = 'fa fa-check';
                titulo = 'Correcto';
                break;
        }

        $.alert({
            icon: icono,
            title: titulo,
            type: color,
            content: msg,
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });
    }


});
