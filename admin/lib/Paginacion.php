<?php
	/**
	*
	*/
	class Paginacion
	{
		function __construct($parametros = array())
		{
			if(count($parametros) > 0)
				$this->iniciar($parametros);
		}

		function iniciar($parametros = array()) {
			foreach ($parametros as $key => $val) {
				$this->$key = $val;
			}
		}

		function crearlinks() {
			$paginacion = '';
			$siguiente = $this->pagina + 1;
			$anterior = $this->pagina - 1;
			$penultima = $this->ultima_pag - 1;

			$paginacion = '';

			if($this->pagina > 1)
				$paginacion .= "<li class='waves-effect'><a href='#!' class='mpag' id='1'><i class='material-icons'>first_page</i></a></li>";
			else
				$paginacion .= "<li class='disabled'><a href='#!'><i class='material-icons'>first_page</i></a></li>";
			if ($this->pagina > 1)
	            $paginacion .= "<li class='waves-effect'><a href='#!' id='".($anterior)."' class='mpag'><i class='material-icons'>chevron_left</i></a></li>";
	        else
	            $paginacion .= "<li class='disabled'><a href='#!'><i class='material-icons'>chevron_left</i></a></li>";

	        if ($this->ultima_pag < 7 + ($this->adyacentes * 2)) {
	            for ($contador = 1; $contador <= $this->ultima_pag; $contador++) {
	                if ($contador == $this->pagina)
	                    $paginacion .= "<li  class='active'><a href='#!'>$contador</a></li >";
	                else
	                    $paginacion .= "<li class='waves-effect'><a href='#!' id='".($contador)."' class='mpag'>$contador</a></li>";
	            }
	        }
	        elseif($this->ultima_pag > 5 + ($this->adyacentes * 2)) {
	            if($this->pagina < 1 + ($this->adyacentes * 2)) {
	                for($contador = 1; $contador < 4 + ($this->adyacentes * 2); $contador++) {
	                    if($contador == $this->pagina)
	                        $paginacion .= "<li  class='active'><a href='#!'>$contador</a></li >";
	                    else
	                        $paginacion .= "<li class='waves-effect'><a href='#!' id='".($contador)."' class=' mpag'>$contador</a></li>";
	                }
	                $paginacion .= "...";
	                $paginacion .= "<li class='waves-effect'><a href='#!' id='".($penultima)."' class='mpag'> $penultima</a></li>";
	                $paginacion .= "<li class='waves-effect'><a href='#!' id='".($this->ultima_pag)."' class=' mpag'>$this->ultima_pag</a></li>";

	           }
	           elseif($this->ultima_pag - ($this->adyacentes * 2) > $this->pagina && $this->pagina > ($this->adyacentes * 2)) {
	               $paginacion .= "<li class='waves-effect'><a href='#!' id='1' class=' mpag'>1</a></li>";
	               $paginacion .= "<li class='waves-effect'><a href='#!' id='2' class=' mpag'>2</a></li>";
	               $paginacion .= "...";
	               for($contador = $this->pagina - $this->adyacentes; $contador <= $this->pagina + $this->adyacentes; $contador++) {
	                   if($contador == $this->pagina)
	                       $paginacion .= "<li  class='active'><a href='#!'>$contador</a></li >";
	                   else
	                       $paginacion .= "<li class='waves-effect'><a href='#!' id='".($contador)."' class=' mpag'>$contador</a></li>";
	               }
	               $paginacion .= "..";
	               $paginacion .= "<li class='waves-effect'><a href='#!' id='".($penultima)."' class=' mpag'>$penultima</a></li>";
	               $paginacion .= "<li class='waves-effect'><a href='#!' id='".($this->ultima_pag)."' class=' mpag'>$this->ultima_pag</a></li>";
	           }
	           else {
	               $paginacion .= "<li class='waves-effect'><a href='#!' id='1;' class=' mpag'>1</a></li>";
	               $paginacion .= "<li class='waves-effect'><a href='#!' id='2;' class=' mpag'>2</a></li>";
	               $paginacion .= "..";
	               for($contador = $this->ultima_pag - (2 + ($this->adyacentes * 2)); $contador <= $this->ultima_pag; $contador++) {
	                   if($contador == $this->pagina)
	                        $paginacion .= "<li  class='active'><a href='#!'>$contador</a></li >";
	                   else
	                        $paginacion .= "<li class='waves-effect'><a href='#!' id='".($contador)."' class=' mpag'>$contador</a></li>";
	               }
	           }
	        }
	        if($this->pagina < $contador - 1)
	            $paginacion .= "<li class='waves-effect'><a href='#!' id='".($siguiente)."' class='mpag'><i class='material-icons'>chevron_right</i></a></li>";
	        else
	            $paginacion .= "<li class='disabled'><a href='#!'><i class='material-icons'>chevron_right</i></a></li>";

			if($this->pagina < $this->ultima_pag)
	            $paginacion .= "<li class='waves-effect'><a href='#!' class='mpag' id='".($this->ultima_pag)."'><i class='material-icons'>last_page</i></a></li>";
	        else
	            $paginacion .= "<li  class='disabled'><a href='#!'><i class='material-icons'>last_page</i></a></li>";

	        return $paginacion;
		}
	}

?>
