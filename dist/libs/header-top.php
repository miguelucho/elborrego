 <div class="Top">
   <div class="Top-int">
     <div class="Top-int-izq">
       <div class="Top-logo">
         <a href="/"> <img src="dist/assets/images/logo.png" alt=""></a>
       </div>
     </div>
     <div class="Top-int-der">
       <div class="Menu-drop Menu-oculto">
         <a href="javascript:void(0)" id="Drop">
           <img src="dist/assets/images/menu.svg" alt="">
         </a>
       </div>
       <div class="Top-menu Menu-completo Menu-completo-oculto">
         <nav>
           <ul class="Menu-togg">
             <li><a href="/">Inicio</a></li>
             <li><a href="nuestra-empresa">Nuestra empresa</a></li>
             <li><a data-scroll href="#servicios">Servicios y proyectos</a></li>
             <li><a data-scroll href="#contacto">Contacto</a></li>
           </ul>
         </nav>
       </div>
     </div>
   </div>
 </div>