<?php
session_start();
if (!isset($_SESSION['elborrego_user'])) {
  header('Location: login');
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Admin | Inversiones el Borrego</title>
  <link rel="stylesheet" href="../dist/css/material-icons.css" />
  <link rel="stylesheet" href="../dist/css/materialize.css" />
  <link rel="stylesheet" href="../dist/css/load.css" />
  <link rel="stylesheet" href="../dist/css/bundled.css" />
  <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="css/jquery.modal.css" />
  <link rel="stylesheet" href="js/croppie/croppie.css" />
  <link rel="stylesheet" href="../dist/css/administrador.css" />
  <style>
    #upload-demo {
      width: 100%;
      height: 400px;
    }

    .select-dropdown {
      max-height: 300px;
    }
  </style>
</head>

<body>
  <nav>
    <?php include("header-admin.php") ?>
  </nav>
  <div class="Contenedor-admin-global">
    <div class="Contenedor-admin-global-int">
      <div class="Contenedor-boton-crear">
        <div class="Btn-flotante-crear">
          <a href="#" data-target="crearproyecto" data-position="left" data-tooltip="Crear Proyectos" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light light-blue darken-2"><i class="material-icons">add</i></a>
        </div>
      </div>
      <div class="Contenedor-admin-titulo">
        <h4 class="Titulh4">Proyectos</h4>
      </div>

      <div class="Contenedor-admin-texto">
        <p>Crear y administra los proyectos, verifica que el estado sea activo para que se puedan visualizar en el Home principal de la pagina.</p>
      </div>

      <div class="Contenedor-admin-texto">
        <div class="Conten-completo">
          <div class="Conten-cuatro">
            <div class="input-field">
              <input id="buscar-nombre" type="text" class="validate">
              <label for="buscar-nombre">Buscar proyecto</label>
            </div>
          </div>
          <div class="Conten-cuatro Margin-top">
            <button class="btn light-blue darken-2" id="busqueda">Buscar</button>
          </div>
        </div>
      </div>

      <div class="Contenedor-admin-tabla">
        <table class="striped">
          <thead>
            <tr>
              <th>Orden</th>
              <th>Nombre</th>
              <th>Fecha de creación</th>
              <th>Fecha de modificación</th>
              <th>Imagen</th>
              <th>Estado</th>
              <th colspan="2">Acciones</th>
            </tr>
          </thead>
          <tbody id="listado-proyectos">
            <tr>
              <td><span>1</span></td>
              <td><span>Aca va el nombre del proyecto si el nombre del proyecto es grande</span></td>
              <td><span>19-08-2021</span></td>
              <td><span>19-08-2021</span></td>
              <td><a class="waves-effect waves-light btn">Ver</a></td>
              <td><span>Activo</span></td>
              <td><a class="waves-effect waves-light btn light-blue darken-2">Editar</a> <a class="waves-effect waves-light btn  red darken-3">Eliminar</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="listado-loader">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div>
            <div class="gap-patch">
              <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="listado-paginacion">
        <ul class="pagination">
        </ul>
      </div>
    </div>
  </div>

  <div id="crearproyecto" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Crear Proyecto</h4>
      <p>Ingresa el nombre del proyecto a crear y selecciona el estado para que pueda ser visualizado en la parte externa de la pagina web.</p>
      <div class="Conten-form-admin">
        <form id="Proyecto-nuevo">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre" type="text" name="proyecto[nombre]" class="validate" required>
                <label for="Nombre">Nombre de proyecto</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="file-field input-field">
                <div class="btn">
                  <span>Imagen</span>
                  <input type="file" class="imagen-adjunta">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Cargar imagen" required>
                </div>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <select name="proyecto[estado]" required>
                  <option value="" disabled selected>Seleccionar</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado del proyecto</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-uno">
              <div class="input-field">
                <label>Ingresar descripción del proyecto</label>
                <br><br>
                <textarea id="Descripcion" class="materialize-textarea" name="proyecto[describe]" required></textarea>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <div id="editarproyecto" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Editar Proyecto</h4>
      <p>Ingresa el nombre del proyecto a editar y selecciona el estado para que pueda ser visualizado en la parte externa de la pagina web.</p>
      <div class="Conten-form-admin">
        <form id="Proyecto-editar">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre-ed" type="text" name="proyecto[nombre]" class="validate">
                <label for="Nombre-ed">Nombre de proyecto</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="file-field input-field">
                <div class="btn">
                  <span>Imagen</span>
                  <input type="file" class="imagen-adjunta">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Cargar imagen">
                </div>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <select name="proyecto[estado]" id="Estado-ed">
                  <option value="" disabled selected>Seleccionar</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado del proyecto</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-uno">
              <div class="input-field">
                <label>Ingresar descripción del proyecto</label>
                <br><br>
                <textarea id="Descripcion-ed" class="materialize-textarea" name="proyecto[describe]"></textarea>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <script src="../dist/js/jquery.min.js"></script>
  <script src="../dist/js/materialize.js"></script>
  <script src="../dist/js/inicializar.js"></script>
  <script src="../dist/js/jquery-confirm.min.js"></script>
  <script src="js/ckeditor/ckeditor.js"></script>
  <script src="js/jquery.modal.min.js"></script>
  <script src="js/croppie/croppie.min.js" type="text/javascript"></script>
  <script src="js/proyectos.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
